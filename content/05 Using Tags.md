
# Using Tags

## Predefined or often used tags:

Below is the list of most common or pre-defined tags.

| Tag   | Kind         | Description                            |
|-------|--------------|----------------------------------------|
| @wip  | predefined   | “Work in Process”                      |
| @skip | predefined   | Skip/disable a feature, scenario, etc  |
| @slow | user-defined | Marks slow, long-running tests         |

A Scenario or Feature can have as multiple tags which are separated by spaces as shown in the example below:

```Gherkin
Feature: testing add Feature
  
  @slow @important @nodata
  Scenario: Check if add task is present.
    Given server is runnning 
    And <User> is already logged in
```

## Selection logic

### Behave 

Following command options to be used for selection/de-selection of testing scenarios

| Logic Operation   | Command Options         | Description                        |
|-------------------|-------------------------|------------------------------------|
| select/enable     | --tags=@one             | Only items with this tag.          |
| not (tilde/minus) | --tags=~@one            | Only items without this tag.       |
| logical-or        | --tags=@one,@two        | If @one or @two is present.        |
| logical-and       | --tags=@one --tags=@two | If both @one and @two are present. |

```cmd
behave --tags=-slow ../features/add.feature
```
