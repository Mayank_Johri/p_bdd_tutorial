
# Environmental Controls

## `behave`

### `environment.py`

As we have seen that context works in different scopes, thus settings from one features are lost in another feature, Behave provides following methods in `enviroment.py` file

#### before_scenario

This function is executed as the name suggest before the `scenario` is exected.

#### after_scenario

This function is executed as the name suggest after the `scenario` is exected.

#### before_feature

This function is executed as the name suggest before the `feature` is exected.

#### after_feature

This function is executed as the name suggest after a `feature` is exected.

#### before_tag

This function is executed as the name suggest before a `tag` is exected.

#### after_tag

This function is executed as the name suggest after a `tag` is exected.

#### before_all

This function is executed as the name suggest before anything is exected.

#### after_all

This function is executed as the name suggest is exected at the end of entire test.

#### before_step

This function is executed as the name suggest before a `step` is exected.

#### after_step

This function is executed as the name suggest after a `step` is exected.

### Example:

```python
# -*- coding: utf-8 -*-
"""."""
from configparser import ConfigParser
import logging


def before_scenario(context, scenario):
    """."""
    context.log.info(
        "\n\n===============================================================\n"
        "Starting scenario: '%s'\n"
        "================================================================"
        % scenario.name)


def after_scenario(context, scenario):
    """."""
    context.log.info(
        "\n===============================================================\n"
        "Finished scenario: '%s'\n"
        "================================================================\n"
        % scenario.name)


def before_feature(context, feature):
    """."""
    pass


def after_feature(context, feature):
    """."""
    pass
    # logging.debug("These run after each feature file is exercised.")


def before_tag(context, tag):
    """."""
    pass
    # logging.debug("These run before a section tagged with the given name. \
    # They are invoked for each tag encountered in the order they’re found \
    # in the feature file. See controlling things with tags.")


def after_tag(context, tag):
    """."""
    pass
    # logging.debug("These run after a section tagged with the given name. \
    # They are invoked for each tag encountered in the order \
    # they’re found in the feature file. See controlling things \
    # with tags.")


def before_all(context):
    """."""
    """
    """
    # get logger
    context.log = logging.getLogger('06.1_feature')
    # create file handler which logs even debug messages
    # overwrite the old log file
    fh = logging.FileHandler(filename='reports/behave.log', mode='w')
    # create console handler with a higher log level
    context.log.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    fh.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    format = '[%(asctime)s] [%(name)s] [%(levelname)s]: %(message)s'
    formatter = logging.Formatter(format, "%a %Y-%m-%d %H:%M:%S %z")
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    context.log.addHandler(ch)
    context.log.addHandler(fh)

    if 'server' in context.config.userdata:
        context.log.debug("server: {context.config.userdata.get('server')}")
        context.userdata = {}
        # Getting the `server` parameter from command line
        # using `-D server=production
        server = context.config.userdata.get('server')
        config = ConfigParser()
        config.read('config.ini')
        context.userdata['url'] = config[server]['url']
        context.userdata['server'] = config[server]['server']
    else:
        print("Missing server")


def after_all(context):
    """."""
    # close db connection
    context.log.debug("Executed after all features")


def before_step(context, step):
    """."""
    context.log.debug("[STEP]: '%s %s'" % (step.keyword, step.name))


def after_step(context, step):
    """."""
    logging.debug("These run after every step.")
```
