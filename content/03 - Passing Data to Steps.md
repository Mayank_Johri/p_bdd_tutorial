
# Passing Data to Steps

Most of the testcases are of no use without the data to test. Feature files provide three ways to pass the data to steps.
One of them is passing **single set of data** from `feature` file to `step definition` `py` file.

In this section we will be discussing about it in details.  

<div style="color: #ffffff ; background: #007584; text-align: center; width:100%; padding: 0.2em;  margin-below: 0.9em">**!!! NOTE !!!**</div><p style="border-bottom: 0.1em solid #007584">
- As `Features` are mostly same for all the frameworks, we will not discuss them separately but will only cover one.  
- As `steps` are differnt for all the frameworks, we will discuss them separately.</p>

## Passing Single set of data

### Passing data as parameter

#### Feature File

In `steps` of feature file, we can pass data, by placing it in `steps` along with text as shown in the example below

```Gherkin
Feature: testing add Feature

  Scenario: run a adding test of two integer
    Given local server is running
    When add 1 and 2
    Then we will return with 3
```    

In the above example, we are passing `local`, `1`, `2` and `3` as data to the steps, but from feature file its very difficult to identify. We have to view the `step` `definition` file to see how data is extracted from the feature file. 

#### Step Definition File

```python
import requests
from behave import given, when, then
from configparser import ConfigParser as CP


@given(u'{url_l} server is running')
def step_server_running(context, url_l):
    config = CP()
    config.read('config.ini')
    url = config[url_l]['url']
    print(url, url_l)
    context.userdata = {}
    context.userdata['url'] = url
    var_r = requests.get(url).json()
    assert var_r == {"result": "true"}


@when(u'add {a} and {b}')
def step_add(context, a, b):
    js_result = {
        'task': 'add',
        'a': a,
        'b': b
    }
    context.numbs = {}
    context.numbs['a'] = a
    context.numbs['b'] = b
    context.results = requests.post(context.userdata['url']+"task",
                                    json=js_result).json()
```

In the `step definition` file, you can see we are extracting data from the feature file using `{var_name}`. In line 

```python
@given('{url_l} server is running') 
``` 
`url_l` will get the data `local` from the `feature file`. Similarly, In line 
```python 
@when(u'add {a} and {b}') 
``` 
variable `a` and `b` will be populated with data `1` & `2` respectivly. 

<div style="color: #ffffff ; background: #007584; text-align: center; padding: 0.2em;  margin-below: 0.9em">**!!! NOTE !!!**</div>
- The step function also has same parameters, for example. 
<br>
---

```python
@given(u'{url_l} server is running')
def step_server_running(context, url_l):
```

In the above example, url_l is present in both the lines. In first line we are letting the python know the location of the data and in the second we are passing it as parameter to the function. 

### Passing as text

One issue with the previous technology is that passing longs texts as parameter is not practical. `context.text` can be used to overcome this issue. 

#### Feature File

In `steps` of feature file, we can pass long text using `docstring` as shown in the example below

```Gherkin
Feature: Echo Feature - Returns the text send in json format

  Scenario: testing echo by sending multiline text
    Given local server is running
    When text is send
      """
      II ॐ भूर्भुवः स्वः तत्सवितुर्वरेण्यं भर्गो देवस्यः धीमहि धियो यो नः प्रचोदयात् II
      O God! You are Omnipresent, Omnipotent and Almighty, You are all Light. 
      You are all Knowledge and Bliss. You are Destroyer of fear, You are Creator of this Universe, 
      You are the Greatest of all. We bow and meditate upon Your light. 
      You guide our intellect in the right direction.
      """
    Then we will return same text
```    

In the above example, we are attaching multi line text to a step `When`, Now lets view the `step definition` file to find how to access the attached text using `context.text`

#### Step Definition File

```python
@when(u'text is send')
def step_text_sent(context):
    """."""
    js = {
        "text": context.text
    }
    context.results = requests.post(context.userdata['url'] + "echo",
                                    json=js).json()
```

## Passing multi-set of data

There are two ways to pass the multi-set of data to the steps.

### Using Table

There are times when you just wanted to pass multiple sets of data to a single step. This methods is good in those cases. Lets try this method in our previous code.

We need to update both `Feature` file and `steps` definition file.  

#### Feature File

```Gherkin
Feature: testing add Feature

  Scenario: run a adding test of two integer
    Given local server is running
    When add two numbers
        | a | b  | c |
        | 1 | 2  | 3 |
        | 5 | -1 | 4 |
    Then we will return code is 200
```

In the `Feature` example above, we have created a table directly under step which starts from `When` in the above example. The table has two `columns` `a` & `b` and data is stored in rows. 

#### Step File

The corresponding `Step` definition looks as shown below

```python 
@when(u'add two numbers')
def step_add(context):
    js = """{{"task": "add",
        "a": {val_a},
        "b": {val_b}
    }}"""
    lst = []
    for a, b, c in context.table:
        json_data = ast.literal_eval(js.format(val_a=int(a),
                                               val_b=int(b)))
        lst.append(requests.post(context.userdata['url'] + "task",
                                 json=json.loads(json_data)).status_code)
    context.result = lst
```

In the above example, we are passing the entire table in single execution and we are using `for` loop to process the table data, which we obtained using `context.table`. 

### Using `Example` keyword

`Example` keyword provide the data in tabular format and is consumed one row at a time. 

#### Feature File

```Gherkin
Feature: testing add Feature

  Scenario Outline: run a adding test of two integer
    Given "local" server is running
    When add <a> and <b>
    Then we will return with <c>

    Examples: integer
        | a | b   | c  |
        | 1 | 2   | 3  |
        | 5 | -1  | 4  |
        | 5 | -11 | 2  |
        | 5 | -11 | -6 |

```

In the above `feature` file, data is stored in tabular format, and it is used by multiple steps, like `a` and `b` are used in `When` and `c` is used in `Then` Step 

<div style="color: #ffffff ; background: #007584; text-align: center; padding: 0.2em;  margin-below: 0.9em">**!!! NOTE !!!**</div> 
- The variable is in between `<` and `>` as shown in the example above. 
- Putting parameters in double-quoted text makes the variation-points enhance visibility. 
<br>
---

#### Step definition file

```python

@when(u'add {a} and {b}')
def step_add(context, a, b):
    js = {
        'task': 'add',
        'a': a,
        'b': b
    }
    context.numbs = {}
    context.numbs['a'] = a
    context.numbs['b'] = b
    context.results = requests.post(context.userdata['url'] + "task",
                                    json=js).json()
```

#### Data with datatype

Data type can also be defined in step implementation, in the code sample below, we have two implementation of same step, one is when data type is `int` and another when it is `float`

```python

@then('we will return with {expected_res:d}')
def step_we_will_return_with_int(context, expected_res):
    actual_res = context.results['result']
    assert float(expected_res) == actual_res, (expected_res, actual_res)


@then('we will return with {expected_res:f}')
def step_we_will_return_with_float(context, expected_res):
    actual_res = context.results['result']
    assert float(expected_res) == actual_res, (expected_res, actual_res)
```

| Type           | Characters matched                                                                                                                       | Output type     |
|----------------|------------------------------------------------------------------------------------------------------------------------------------------|-----------------|
| w              | Letters and underscore                                                                                                                   | str             |
| W              | Non-letter and underscore                                                                                                                | str             |
| s              | Whitespace                                                                                                                               | str             |
| S              | Non-whitespace                                                                                                                           | str             |
| d              | Digits (effectively integer numbers)                                                                                                     | int             |
| D              | Non-digit                                                                                                                                | str             |
| n              | Numbers with thousands separators (, or .)                                                                                               | int             |
| %              | Percentage (converted to value/100.0)                                                                                                    | float           |
| f              | Fixed-point numbers                                                                                                                      | float           |
| e              | Floating-point numbers with exponent e.g. 1.1e-10, NAN (all case insensitive)                                                            | float           |
| g              | General number format (either d, f or e)                                                                                                 | float           |
| b              | Binary numbers                                                                                                                           | int             |
| o              | Octal numbers                                                                                                                            | int             |
| x              | Hexadecimal numbers (lower and upper case)                                                                                               | int             |
| ti             | ISO 8601 format date/time e.g. 1972-01-20T10:21:36Z (“T” and “Z” optional)                                                               | datetime        |
| te             | RFC2822 e-mail format date/time e.g. Mon, 20 Jan 1972 10:21:36 1000                                                                      | datetime        |
| tg             | Global (day/month) format date/time e.g. 20/1/1972 10:21:36 AM 1:00                                                                      | datetime        |
| ta             | US (month/day) format date/time e.g. 1/20/1972 10:21:36 PM 10:30                                                                         | datetime        |
| tc             | ctime() format date/time e.g. Sun Sep 16 01:03:52 1973                                                                                   | datetime        |
| th             | HTTP log format date/time e.g. 21/Nov/2011:00:07:11 +0000                                                                                | datetime        |
| ts             | Linux system log format date/time e.g. Nov 9 03:37:44                                                                                    | datetime        |
| tt             | Time e.g. 10:21:36 PM -5:30                                                                                                              | time            |
| MathExpression | Mathematic expression containing: [0-9 +-*/%.e]+                                                                                         | float           |
| QuotedString   | String inside double quotes (“). Double quotes inside the string can be escaped with a backslash                                         | text w/o quotes |
| Boolean        | Boolean value: True: 1, y, Y, yes, Yes, YES, true, True, TRUE, on, On, ON False: 0, n, N, no, No, NO, false, False, FALSE, off, Off, OFF | bool            |
