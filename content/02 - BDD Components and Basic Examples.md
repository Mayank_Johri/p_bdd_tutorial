
# BDD Components and Basic Examples

Lets start our testing, for features we need to have a "features" folder, so lets create one. 

Next create a new directory called "features/steps". In this directory we are going to keep all our steps in different files.

## Folder structure or layout

For BDD, we will use the following folder structure. 

```
<test_project_root>
|--> configs.py
|--> data.txt
|--> features
     |
     |--> divide.features
     |--> add.features
     |--> delete.features
     |--> multi.features
     |--> steps
          |--> add_steps.py
          |--> del_steps.py
```

We can divide the entire folder strucutre in three parts. 
- Core Folder
- Features
- Steps Folder

### Root Folder

In core folder, environment, config etc files should be created & stored.

## Feature File

`.feature` files conventionally consists of a single feature and one or more Scenario's. It is a single file that typically defines a single story. 

The File starting lines (it may or may not be first line) starts with the keyword `Feature:` followed by text describing the feature under test. In the next line we can have a list of text briefly describing the scenarios to test.

<center style="border: 1px solid #007584"><div style="color: #ffffff ; background: #007584; text-align: center;">**!!! NOTE !!!**</div>
<div style="background: #f4fdff; padding-top: 0.6em">Free Text is Free text, Use it wisely, Keep it simple and short.</div></center>

The `Feature` under testing will have more then one scenarios to validate. They can be defined using the keyword `Scenarios` (or its localized equivalent as Gherkin is localized for many languages) on a new line. 

Every `scenario` consists of a sequence of `steps`. All `steps` **must start** with one of the following keywords
- Given 
- When 
- Then
- But
- And 

Here is an example of a feature file to test the addion feature of a `REST` based calculator:

```Gherkin
Feature: testing add Feature

  Scenario: Check if add task is present.
    Given local server is running
    When add 11 and 22
    Then we will return with 33


  Scenario Outline: run a adding test of two integer
    Given local server is running
    When add <a> and <b>
    Then we will return with <c>

    Examples: integer
        | a | b   | c  |
        | 0 | 0   | 0  |
        | 1 | 2   | 3  |
        | 5 | -1  | 4  |
        | 5 | -11 | 2  |
        | 5 | -11 | -6 |

    Scenario Outline: run a adding test of two floats
      Given local server is running
      When add <a> and <b>
      Then we will return with <c>

      Examples: integer
          | a     | b    | c    |
          | 1.1   | 2.2  | 3.3  |
          | 5.6   | -1.0 | 4.6  |
          | 20.01 | -11  | 9.01 |
          | 52    | 11   | 63   |
```

<div style="color: #ffffff ; background: #007584; text-align: center; padding: 0.2em">**!!! NOTE !!!**</div>

- Cucumber/Behave etc treats the step `keywords` same, but its a good idea to have meaningful keywords in `steps` as they will be used by developers to develop the scenarios and incorrect one **will** cause issues.

- You can use tagging to group features and scenarios together independent of your file and directory structure.


### Descriptions

`Gherkin` allows to have few parts of documents do not have to start with a `keyword`. They usually used to describe in free text and not worry about it being processed.
They are usually followed a `Feature`, `Scenario`, `Scenario Outline` or `Examples`.

<div style="color: #ffffff ; background: #007584; text-align: center; padding: 0.2em">**!!! NOTE !!!**</div>
- Descriptions should not have line starting with any `keyword`

## Steps

A `step` starts with a `defined` keywords (`Given`, `When`, `Then`, `But` and `And`). `And` or `But` are used when multiple `Given` or `When` steps are used adjacent to each other. 

`Gherkin` when parsing/executing does not differentiate between the keywords, but for readability of the scenario and steps, choosing the right one is very important.

### ‘Given’

`Given` steps are used to describe the initial state/context of the system. It is typically something that happened in the past and is pre-requisite for the schenario under discussion.

When it is executed, the system is configured to a well-defined state, such as creating & configuring objects or adding data to the test database. Like, opening the browser and navigating to a particular section for testing.

We can have several `Given` steps with `And` or `But` between them to increase the readability.

### When

`When` steps are used to describe an action or the event. It can be a person interacting with the system, or it can be an event triggered by another system.

<div style="color: #ffffff ; background: #007584; text-align: center; padding: 0.2em">**!!! NOTE !!!**</div>
- Please have only have a single `When` step per `scenario`. 
- If not possible than consider to split the `scenario` up in multiple `scenario`

### ‘Then’

`Then` step describe the expected outcome of the previous action.

Definition of a `Then` step should be used to `assert` **actual** outcome with **expected** outcome.

### And 

`And` step describes joining relationship between past step and current step.

We can have multiple `And` steps in consequent steps. 

__Example__:
```Gherkin
Feature: testing add Feature

  Scenario: Check if add task is present.
    Given server is runnning 
    And <User> is already logged in
```

### But

`But` steps describes joining relationship between past `step` and current `step` similar to `And`, except its meaning is different. 

### Background

`Backgrounds` allows to add some context to all `scenarios` in the `feature` file they are described. It's like an `untitled scenario` with steps. The background is executed before each `scenarios`, after `BeforeScenario` `hooks`.

### Scenario Outline

It provides **parametrized scenario script** for the `feature` file. It is executed for each row in the `Examples` section below the `Scenario Outline`.

With the help of `Examples`, `tables` and `< > delimited parameters` `Scenario outlines` allows more concisely express test cases to be created. 

`steps` under Scenario Outline` are never directly executed, but instead executed once for each `row` in the `Examples` section beneath it. First row of `Example` act as row name.

#### Difference between `Scenario` and `Scenario Outline`

One key difference between them is that `Scenario` cannot have multiple data or have access to `Examples`, where as with the help of `Examples` and `Tables` `Scenario Outline` can be tested against multiple datasets.

- `Scenario` states the general point of test in more abstract way. Executes only once 
- `scenario outline` facilitates performing scenario with one/more examples. Depending on data in `Example` can run multiple times. 

## Radish Specific 

### Scenario Loop

It is similar to standard `Scenario` with one difference, that it is repeated for a given number of `iterations`. It is mostly useful during `stabilization` tests are performed. 

`Scenario Loops` have the following syntax:

__Example__

```Gherkin
Feature: Testing Add Feature

  Scenario Loop 5: Check if add task is present.
    Given server is runnning 
    And <User> is already logged in
```

### Scenario Precondition

There are times, when we have a situation, where one `Scenario` is depended of one or more existing `Scenario`'s. `Radish` provide's a mechanism called `Scenario Precondition` which allows to define the depending `Scenarios`. 

Every Scenario can be used as a Precondition Scenario. Scenario Preconditions are implemented as special tags:

__Example__

```Gherkin
Feature: Testing Add Feature


  @precondition(OtherFeatureFile.feature: OtherScenario)
  Scenario Loop 5: Check if add task is present.
    Given server is runnning 
    And <User> is already logged in
```

In the above example, `radish` will import the `Scenario` with the sentence `OtherScenario` from the feature file `OtherFeatureFile.feature` and run it before the `Check if add task is present` Scenario.

<div style="color: #ffffff ; background: #007584; text-align: center; padding: 0.2em">**!!! NOTE !!!**</div>
    - Scenario Preconditions are not standard gherkin

### Examples

Explained later (Chapter 3)

## Extra Keywords

### """ (Doc Strings)

Explained later

###   | (Data Tables)

Explained later 

### @ (Tags)

Tags are a great way to organize your features and scenarios. They can be used to club the features and scenarios and execution selection can be regulated using them.  

### # (Comments)

single line comments

## Step definition File 

Step definition file consists of the implementation of the steps used in feature files. A sample steps, implementation using `behave`, file for previous feature file is as follows. 

```python
import requests
from behave import given, when, then
from configparser import ConfigParser as CP


@given(u'{url_l} server is running')
def step_server_running(context, url_l):
    config = CP()
    config.read('config.ini')
    url = config[url_l]['url']
    print(url, url_l)
    context.userdata = {}
    context.userdata['url'] = url
    var_r = requests.get(url).json()
    assert var_r == {"result": "true"}


@when(u'add {a} and {b}')
def step_add(context, a, b):
    js_result = {
        'task': 'add',
        'a': a,
        'b': b
    }
    context.numbs = {}
    context.numbs['a'] = a
    context.numbs['b'] = b
    context.results = requests.post(context.userdata['url']+"task",
                                    json=js_result).json()


@then(u'we will return with {expected_res:d}')
def step_we_will_return_with(context, expected_res):
    actual_res = context.results['result']
    assert float(expected_res) == actual_res, (expected_res, actual_res)


@then(u'we will return with {expected_res:f}')
def step_we_will_return_with(context, expected_res):
    actual_res = context.results['result']
    assert float(expected_res) == actual_res, (expected_res, actual_res)
```

<div style="color: #ffffff ; background: #ff3300; text-align: center;">**!!! Warning !!!**</div>
- No two steps should be same.
- Steps should be designed in such a way that they can handle all the instances of it from the feature files.

## Running all files

### Behave

In the root folder run the following command to execute all the feature files in default locations

```
behave
```

### Radish

Radish allows to execte it against folders, which can contain multiple features in subdirectories.

```
radish features
```

> **or** 

```
radish .
```

### Aloe

The aloe helper runs Nose with the Aloe plugin enabled.

- `--with-gherkin` argument to run `BDD` tests
- `--no-ignore-python` argument will discover & run other `nose` discovered tests as well

The `aloe` command line tool is a wrapper for the `nose runner`, configured to only run `Gherkin` tests. As such, the invocation is the same as `nose`, with following parameters added:

- -n N[,N...] - only run the specified scenarios (by number, 1-based) in each feature. Makes sense when only specifying one feature to run, for example

`aloe features/calculator.feature -n 1`

- --test-class - override the class used as a base for each feature.

- --no-ignore-python - run Python tests as well as Gherkin.



```
aloe
```

### Lettuce

```
lettuce features/
```

## Running 1 Feature file

### Behave

In the root folder run the following command to execute all the feature files in default locations

```
behave features/test.feature
```

### radish-bdd

```
radish features/cal.feature 
```

```Gherkin
Feature: My first feature file using radish  # features/cal.feature
    In order to test my awesome software
    I need an awesome BDD tool like radish
    to test my software.

    Scenario: Test my calculator
        Given I have the numbers 5 and 6
        When I sum them
        Then I expect the result to be 11

    Scenario Outline: Test my calculator with examples
        Given I have the numbers <a> and <b>
        When I sum them
        Then I expect the result to be <c>

    Examples:
        | a | b   | c  |
        | 0 | 0   | 0  |
        | 1 | 2   | 3  |
        | 5 | -1  | 4  |
        | 5 | -11 | 2  |
          AssertionError: 
        | 5 | -11 | -6 |

1 features (0 passed, 1 failed)
6 scenarios (5 passed, 1 failed)
18 steps (17 passed, 1 failed)
Run 1520553579 finished within a moment
```

### Lettuce

```
lettuce features/test.feature
```

### Aloe

```
aloe features/test.feature -n 1
```

## Displaying Output

To display the `print`, use the following options on `behave`

```
behave --no-capture features_1/add_cal.feature 
```

## Referencess

- https://cucumber.io/docs/reference
- http://lettuce.it/tutorial/simple.html#lettuce-it
- http://docs.behat.org/en/v2.5/guides/1.gherkin.html
- https://github.com/cucumber/cucumber/wiki/Scenario-Outlines
- https://github.com/cucumber/cucumber/wiki/Feature-Introduction
- **http://pages.andrew.premdas.org/2011/06/27/composable-features-and-tables.html**
- https://stackoverflow.com/questions/44619535/what-is-the-benefit-of-scenario-over-scenario-outline-in-cucumber
