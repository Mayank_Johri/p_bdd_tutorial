
# Sharing Data Between Steps

## Using `context`

### What is `context`

**According to**: https://github.com/behave/behave/blob/master/docs/tutorial.rst#id30

You'll have noticed the "context" variable that's passed around. It's a clever place where you and behave can store information to share around. It runs at three levels, automatically managed by behave.

When behave launches into a new feature or scenario it adds a new layer to the context, allowing the new activity level to add new values, or overwrite ones previously defined, for the duration of that activity. These can be thought of as scopes.

You can define values in your environmental controls file which may be set at the feature level and then overridden for some scenarios. Changes made at the scenario level won't permanently affect the value set at the feature level.

You may also use it to share values between steps. For example, in some steps you define you might have:

### Creating variables to pass data

`context` variable is shared between `steps`, thus can be used to share data between steps as shown in the example below. 

```python
@given('{url_l} server is running')
def step_server_running(context, url_l):
    """."""
    config = ConfigParser()
    config.read('config.ini')
    context.userdata = {}
    context.userdata['url'] = config[url_l]['url']
    var_r = requests.get(context.userdata['url'] ).json()
    assert var_r == {"result": "true"}


@when(u'text is send')
def step_text_sent(context):
    """."""
    js = {
        "text": context.text
    }
    context.results = requests.post(context.userdata['url'] + "echo",
                                    json=js).json()
```

In the above example, we are using & passing the values on dictionary `context.userdata` between the steps. 

<div style="color: #ffffff ; background: #007584; text-align: center; padding: 0.2em">**!!! NOTE !!!**</div>
- context values can be shared between `steps` of the same `feature`
- user defined context values within a `feature` will be lost between `features`, meaning that values defined in one `feature` cannot be viewed by another `feature` 

## Context and Environment

The `environment.py` module define's the code to execute **before** and **after** certain events during testing:

- **before_step(context, step), after_step(context, step)**: Executes before and after every step.
- **before_scenario(context, scenario), after_scenario(context, scenario)**: Executes before and after each scenario.
- **before_feature(context, feature), after_feature(context, feature)**: Executes before and after each feature file is exercised.
- **before_tag(context, tag), after_tag(context, tag)**: Executes before and after a section tagged with the given name. They are invoked for each tag encountered in the order they're found in the feature file.
- **before_all(context), after_all(context)**: Executes before and after the whole shooting match.

`context` set in `before_feature` & `after_feature` can be used to share the data between `features` as well

## Reference 
- https://github.com/behave/behave/blob/master/docs/tutorial.rst#id30
