
# BDD and Python

## Introduction to `BDD`

`BDD` or `Behavior Driven Development` is a devlopment methodology which allows developers/business users/QA to convert vague requiments to actionable testcases scenarios using the principle of TDD, without having to know details about the testing mechanisms.

or you can say that `Behavior-Driven Development` (`BDD`) is a subset of `Test-Driven Development` (`TDD`) as it works on the principles of `TDD`, in which you write testcases first and then create your code to pass all the testcases. 

Thus it can be said that `BDD` encourages collaboration between developers, QA and business participants in a software project.

### BDD focuses on

- Where to start in the process
- What to test and what not to test
- How much to test in one go
- What to call the tests
- How to understand why a test fails

## Principles of BDD

`TDD` is a software development methodology in which, for each unit of software, a software developer must:

- first define a test set for the unit;
- make sure the tests fail;
- then implement/develop the unit;
- verify that the implementation of the unit makes the tests pass.

One of the issues of TDD was that it allows tests in terms of high-level software requirements, low-level technical details or anything in between. 

BDD tries to enhance TDD by making more specific choices than TDD. It specifies that tests of any unit of software should be specified in terms of the desired behavior of the unit. The "desired behavior" in a case consists of the requirements set by the business — that is, the desired behavior that has business value for whatever entity commissioned the software unit under construction. Within BDD practice, this is referred to as BDD being an "outside-in" activity

### Behavioral specifications

After the desired behavior is finalized, its time to finalized `how` the `desired behavior` is specified.

In this `BDD` chooses to use a semi-formal format for behavioral specification which is usually copied from user story specifications from the field of object-oriented analysis and design.

The scenario aspect of this format may be regarded as an application of `Hoare` logic to behavioral specification of software units using the Domain Language of the situation.

Business Analysts and developers collaborate in this area and specify the behavior in terms of user stories, which are each explicitly written down in a dedicated document.

Each user story should roughly, follow the following structure:




**Title**: The story should have a clear, explicit title.

**Narrative:**

A short, introductory section that specifies
- **who**: (which business or project role) is the driver or primary stakeholder of the story (the actor who derives business benefit from the story)
- **what**: effect the stakeholder wants the story to have
- **why**: business value the stakeholder will derive from this effect

**Acceptance criteria or scenarios:**

A description of each specific case of the narrative. Such a scnario has the following structure:
- It starts by specifying the initial condition that is assumed to be true at the beginning of the scenario. Thismay consist of a single clause, or several.
- It then states which event triggers the start of the scenario.
- Finally, it states the expected outcome, in one or more clauses.

BDD does not have any formal requirements for exactly how these user stories must be written down, but it does insist that each team using BDD come up with a simple, standardized format for writing down the user stories which includes the elements listed above. 

Dan North suggested a template for a textual format which has found wide following in different BDD software tools. A very brief example of this format might look like this:

```Gherkin
Feature: Testing "Add" Feature

  Scenario: Check if add task is present.
    Given local server is running
    When add 11 and 22
    Then we will return with 33


  Scenario Outline: run a adding test of two integer
    Given local server is running
    When add <a> and <b>
    Then we will return with <c>

    Examples: integer
        | a | b   | c  |
        | 0 | 0   | 0  |
        | 1 | 2   | 3  |
        | 5 | -1  | 4  |
        | 5 | -11 | 2  |
        | 5 | -11 | -6 |

    Scenario Outline: run a adding test of two floats
      Given local server is running
      When add <a> and <b>
      Then we will return with <c>

      Examples: float
          | a     | b    | c    |
          | 1.1   | 2.2  | 3.3  |
          | 5.6   | -1.0 | 4.6  |
          | 20.01 | -11  | 9.01 |
          | 52    | 11   | 63   |
```

## Introduction to `Gherkin`

It is a **Business Readable**, **Domain Specific** Language that lets one describe software's behaviour without detailing its implementation.

It serves two purposes 
- documentation and 
- automated tests 

Like `Python` and `YAML`, `Gherkin` is a line-oriented language that uses indentation to define structure. Line endings terminate statements (eg, `steps`). Either `spaces` or `tabs` may be used for indentation (but spaces are more portable). Most lines start with a keyword.

Comment lines are allowed anywhere in the file. They begin with zero or more spaces, followed by a hash sign (`#`) and some amount of text.

The parser divides the input into `features`, `scenarios`, `Scenario Outline`, `Examples`, `Table` and `steps`. When you run the feature the trailing portion (after the keyword) of each step is matched to the `code block` called **Step Definitions**.

## Libraries available in Python

Python has many 3<sup>rd</sup> party libraries are available to implement testcases in BDD. Few of the most command are as follows

- Aloe - Proposed (will cover in later ebook versions)
- Behave - **Done**
- Redish - **WIP**
- pytest-bdd - Proposed (will cover in later ebook versions)
- Lettuce - Will not cover, but instead will cover aloe

We will try to give basics of all of them and in later version will try to create a framework based on it. In this chapter we will discuss about them in brief and will install them.

### behave

Behave can be downloaded from either

- Using pip: 
    > Since I am using `Funtoo Linux`, and since I am trying to install it on `Python3`, my `pip` command is `pip3`. For most of you it will be `pip`.
    
    > Also, `--user` options means that they are installed in my user context and are not available to other users. 
    > You might also wish to have `virtualenv` set for your project in that case, `--user` context is not needed. 

```
$pip3 install behave --user
Collecting behave
  Downloading behave-1.2.6-py2.py3-none-any.whl (136kB)
    100% |████████████████████████████████| 143kB 304kB/s 
Requirement already satisfied: six>=1.11 in /home/mayank/.local/lib64/python3.6/site-packages (from behave)
Collecting parse>=1.8.2 (from behave)
  Downloading parse-1.8.2.tar.gz
Collecting parse-type>=0.4.2 (from behave)
  Downloading parse_type-0.4.2-py2.py3-none-any.whl
Installing collected packages: parse, parse-type, behave
  Running setup.py install for parse ... done
Successfully installed behave-1.2.6 parse-1.8.2 parse-type-0.4.2
```

or, use the **bleeding edge** version from `git` directly using following command 

`pip install git+https://github.com/behave/behave`

> Note: Use the git version if you are confident and are willing to get your hands dirty, else please use the normal pip version.  

### radish

https://github.com/radish-bdd/radish

```
pip3 install radish-bdd --user
```

### pytest-bdd

```
$pip3 install pytest-bdd --user
Collecting pytest-bdd
  Downloading pytest-bdd-2.20.0.tar.gz (65kB)
    100% |████████████████████████████████| 71kB 166kB/s 
Collecting glob2 (from pytest-bdd)
  Downloading glob2-0.6.tar.gz
Collecting Mako (from pytest-bdd)
  Using cached Mako-1.0.7.tar.gz
Requirement already satisfied: parse in /home/mayank/.local/lib64/python3.6/site-packages (from pytest-bdd)
Requirement already satisfied: parse_type in /home/mayank/.local/lib64/python3.6/site-packages (from pytest-bdd)
Requirement already satisfied: pytest>=2.8.1 in /home/mayank/.local/lib64/python3.6/site-packages (from pytest-bdd)
Requirement already satisfied: six>=1.9.0 in /home/mayank/.local/lib64/python3.6/site-packages (from pytest-bdd)
Requirement already satisfied: MarkupSafe>=0.9.2 in /home/mayank/.local/lib64/python3.6/site-packages (from Mako->pytest-bdd)
Requirement already satisfied: setuptools in /home/mayank/.local/lib64/python3.6/site-packages (from pytest>=2.8.1->pytest-bdd)
Requirement already satisfied: pluggy<0.7,>=0.5 in /home/mayank/.local/lib64/python3.6/site-packages (from pytest>=2.8.1->pytest-bdd)
Requirement already satisfied: attrs>=17.2.0 in /home/mayank/.local/lib64/python3.6/site-packages (from pytest>=2.8.1->pytest-bdd)
Requirement already satisfied: py>=1.5.0 in /home/mayank/.local/lib64/python3.6/site-packages (from pytest>=2.8.1->pytest-bdd)
Installing collected packages: glob2, Mako, pytest-bdd
  Running setup.py install for glob2 ... done
  Running setup.py install for Mako ... done
  Running setup.py install for pytest-bdd ... done
Successfully installed Mako-1.0.7 glob2-0.6 pytest-bdd-2.20.0
```

### Aloe

https://github.com/aloetesting/aloe

```
pip3 install aloe --user
Collecting aloe
  Downloading aloe-0.1.15.tar.gz (93kB)
    100% |████████████████████████████████| 102kB 138kB/s 
Collecting ansicolors>=1.1.8 (from aloe)
  Downloading ansicolors-1.1.8-py2.py3-none-any.whl
Collecting colorama>=0.3.9 (from aloe)
  Downloading colorama-0.3.9-py2.py3-none-any.whl
Requirement already satisfied: future>=0.14.0 in /home/mayank/.local/lib64/python3.6/site-packages (from aloe)
Collecting gherkin-official<5,>=4.1.3 (from aloe)
  Downloading gherkin-official-4.1.3.tar.gz
Requirement already satisfied: nose in /home/mayank/.local/lib64/python3.6/site-packages (from aloe)
Collecting repoze.lru (from aloe)
  Downloading repoze.lru-0.7-py3-none-any.whl
Installing collected packages: ansicolors, colorama, gherkin-official, repoze.lru, aloe
  Running setup.py install for gherkin-official ... done
  Running setup.py install for aloe ... done
Successfully installed aloe-0.1.15 ansicolors-1.1.8 colorama-0.3.9 gherkin-official-4.1.3 repoze.lru-0.7
```

### Lettuce

http://lettuce.it/

```
pip3 install Lettuce --user
Collecting Lettuce
  Downloading lettuce-0.2.23.tar.gz (56kB)
    100% |████████████████████████████████| 61kB 141kB/s 
Collecting sure (from Lettuce)
  Downloading sure-1.4.7-py3-none-any.whl
Collecting fuzzywuzzy (from Lettuce)
  Downloading fuzzywuzzy-0.16.0-py2.py3-none-any.whl
Collecting python-subunit (from Lettuce)
  Downloading python_subunit-1.2.0-py2.py3-none-any.whl (112kB)
    100% |████████████████████████████████| 122kB 347kB/s 
Collecting mock (from sure->Lettuce)
  Downloading mock-2.0.0-py2.py3-none-any.whl (56kB)
    100% |████████████████████████████████| 61kB 1.1MB/s 
Requirement already satisfied: six in /home/mayank/.local/lib64/python3.6/site-packages (from sure->Lettuce)
Collecting testtools>=0.9.34 (from python-subunit->Lettuce)
  Downloading testtools-2.3.0-py2.py3-none-any.whl (184kB)
    100% |████████████████████████████████| 194kB 589kB/s 
Collecting extras (from python-subunit->Lettuce)
  Downloading extras-1.0.0-py2.py3-none-any.whl
Collecting pbr>=0.11 (from mock->sure->Lettuce)
  Using cached pbr-3.1.1-py2.py3-none-any.whl
Collecting python-mimeparse (from testtools>=0.9.34->python-subunit->Lettuce)
  Downloading python_mimeparse-1.6.0-py2.py3-none-any.whl
Collecting traceback2 (from testtools>=0.9.34->python-subunit->Lettuce)
  Downloading traceback2-1.4.0-py2.py3-none-any.whl
Collecting fixtures>=1.3.0 (from testtools>=0.9.34->python-subunit->Lettuce)
  Downloading fixtures-3.0.0-py2.py3-none-any.whl (67kB)
    100% |████████████████████████████████| 71kB 680kB/s 
Collecting unittest2>=1.0.0 (from testtools>=0.9.34->python-subunit->Lettuce)
  Downloading unittest2-1.1.0-py2.py3-none-any.whl (96kB)
    100% |████████████████████████████████| 102kB 938kB/s 
Collecting linecache2 (from traceback2->testtools>=0.9.34->python-subunit->Lettuce)
  Downloading linecache2-1.0.0-py2.py3-none-any.whl
Collecting argparse (from unittest2>=1.0.0->testtools>=0.9.34->python-subunit->Lettuce)
  Downloading argparse-1.4.0-py2.py3-none-any.whl
Installing collected packages: pbr, mock, sure, fuzzywuzzy, python-mimeparse, extras, linecache2, traceback2, fixtures, argparse, unittest2, testtools, python-subunit, Lettuce
  Running setup.py install for Lettuce ... done
Successfully installed Lettuce-0.2.23 argparse-1.4.0 extras-1.0.0 fixtures-3.0.0 fuzzywuzzy-0.16.0 linecache2-1.0.0 mock-2.0.0 pbr-3.1.1 python-mimeparse-1.6.0 python-subunit-1.2.0 sure-1.4.7 testtools-2.3.0 traceback2-1.4.0 unittest2-1.1.0
```

<div style="color: #ffffff ; background: #ff3300; text-align: center;">**!!! NOTE !!!**</div>
- `Letture` still not able to support `Python 3`, there is a `Aloe` which is enancement over `letture`

### Misc Libraries

#### Selenium 

```
pip3 install selenium --user
```

#### Flask

```
pip3 install flask --user
```

#### Requests

```
pip3 install requests --user
```

**References:**
    - http://radish-bdd.io/#gettingstarted
    - https://www.agilealliance.org/glossary/bdd/
    - https://github.com/cucumber/cucumber/wiki/Gherkin
    - https://en.wikipedia.org/wiki/Behavior-driven_development
    - http://tott-meetup.readthedocs.io/en/latest/sessions/behave.html
    - https://code.tutsplus.com/tutorials/behavior-driven-development-in-python--net-26547
    - https://www.merixstudio.com/blog/introduction-methodology-behavior-driven-development/
