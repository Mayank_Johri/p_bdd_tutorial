
# Introduction
---

This is my first attempt at learning BDD based testing frameworks. I have tried to document most of the knowledge which I have gathered.

As this is the first version of the book, thus will have lot of things left to be desired. 

The documentation can be downloaded from https://github.com/mayankjohri/LetsExploreBDDUsingPython. 

# BDD And TDD
---

## TDD 

Test-driven development (TDD) is a software development process that relies on the repetition of a very short development cycle. In it the user requirements (user stories) are converted into very specific test cases, testing one specific item of user story, then the software is improved/updated to a level which just pass these new tests. Only after that refectoring is done to improve the updated software while making sure that all tests now pass. 

In TDD software develop first develop the testcases and then make the changes in software just bare enough to pass these testcases and after that the refactoring is done. 

### TDD development lifecycle

#### Add a test

In `TDD` based development, development of every new `feature` begins with writing a `test` for that feature. To write the `test`, the developer must clearly understand the `feature's` `specification` and `requirements`. The developer can accomplish this through `use cases` and `user stories` to cover the `requirements` and `exception conditions`, and can write the test in whatever `testing framework` is appropriate to the software environment. 

This is a `differentiating` feature of test-driven development versus writing unit tests after the code is written: it makes the developer focus on the requirements before writing the code, a subtle but important difference.

#### Run all tests and see if the new test fails

Next step is to execute the existing testing suite along with added testcases. The new testcases should fail, thus raising the requirement to update the code base. 
It also validates that new/updates testcases have significantly changed and will not pass by default. 

#### Write the code

After the testcases have been updated, the development starts and just enough efforts is made the make sure that test cases are passed.

#### Run tests

Again run the testcases, which validates the code is as per the requirements. If any testcases fail then, update the code and try again. 

####  Refactor code

Once all the testcases have passed, only then refactoring and optimization of code happens.

#### Repeat

Now, the developer can start is next feature and start the whole process again for that feature,

## BDD

BDD is an enhancement to TDD in a way that requirement (Features & Scenarios) is created in such as way that they can be directly used to create the testcases. 

The Features and scenarios with steps are written by either developer or Product owner.

We will discuss BDD in details in next chapters.

## TDD and ATDD

Test-driven development is related to, but different from acceptance test–driven development (ATDD). TDD is primarily a developer's tool to help create well-written unit of code (function, class, or module) that correctly performs a set of operations. ATDD is a communication tool between the customer, developer, and tester to ensure that the requirements are well-defined. TDD requires test automation. ATDD does not, although automation helps with regression testing. Tests used in TDD can often be derived from ATDD tests, since the code units implement some portion of a requirement. ATDD tests should be readable by the customer. TDD tests do not need to be.

## TDD and BDD

BDD (behavior-driven development) combines practices from TDD and from ATDD. It includes the practice of writing tests first, but focuses on tests which describe behavior, rather than tests which test a unit of implementation. Tools such as Mspec and Specflow provide a syntax which allow non-programmers to define the behaviors which developers can then translate into automated tests.

## References 
- https://en.wikipedia.org/wiki/Test-driven_development
- http://www.methodsandtools.com/archive/archive.php?id=20
- http://msdn.microsoft.com/en-us/magazine/cc163665.aspx
- http://msdn.microsoft.com/en-us/library/ms379625(VS.80).aspx
